const inventory = require('./inventory');

function lastCarofInventory(inventory){
    if(Array.isArray(inventory)){
        for(let car of inventory){
            let lastCar = inventory[inventory.length-1];
            return `Last car is a ${lastCar.car_make} model is ${lastCar.car_model}`;
        }
    }
    else{
        return null
    }
}
console.log(lastCarofInventory(inventory));

module.exports = lastCarofInventory;
