const inventory = require('./inventory.js');

function allyearsOfCars(inventory){
    if(Array.isArray(inventory)){
        let years = ""
        for(let car of inventory){
            years += car.car_year + " ";
        }
        return years.split(" ");
    }else{
        return null;
    }
    
}
console.log(allyearsOfCars(inventory)); 

module.exports = allyearsOfCars;
