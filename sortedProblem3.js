const inventory = require('./inventory');

function sortCarsAplhabetically(inventory){
    if(Array.isArray(inventory)){
        let carNames = "";
        for(let car of inventory){
            carNames += car.car_model + " ";
        }
        return carNames.split(" ").sort().join();
    }
    else{
        return null
    }
    
}
console.log(sortCarsAplhabetically(inventory));

module.exports = sortCarsAplhabetically;
