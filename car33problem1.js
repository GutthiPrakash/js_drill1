const inventory = require('./inventory.js');

function car33Details(inventory){
    if(Array.isArray(inventory)){
        for(let car of inventory){
            if(car.id === 33){
                return `Car ${car.id} is a ${car.car_make} car year ${car.car_year} car model ${car.car_model}`;
            }
        }
    }
    else{
        return null
    }
    
}
console.log(car33Details(inventory));

module.exports = car33Details; 
